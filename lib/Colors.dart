import 'package:flutter/material.dart';

final lightBlack = Color.fromRGBO(60, 45, 75, 1);
final indigo = Color.fromRGBO(140, 48, 233, 1);
final mainColor = Color.fromRGBO(247, 34, 72, 1);
final white = Colors.white;
final black = Colors.black;
final black45 = Colors.black45;
final grey = Colors.grey;
final grey700 = Colors.grey.shade700;
final red = Colors.red;
final greyLight = Color.fromRGBO(244, 244, 245, 1);
final gold = Color.fromRGBO(255, 190, 91, 1);
final lightBlue = Color.fromRGBO(95, 123, 247, 1);
