import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:group_radio_button/group_radio_button.dart';

import '../../Auth/SignUp/singup_cubit.dart';
import '../../Colors.dart';
import '../../Home/HomePage.dart';

class SingUpPage extends StatefulWidget {
  @override
  _SingUpPageState createState() => _SingUpPageState();
}

class _SingUpPageState extends State<SingUpPage> {
  TextEditingController _userNameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _passwordController1 = TextEditingController();

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formsKey = GlobalKey<FormState>();

  bool _isHide = true;
  bool _isHide1 = true;

  String _verticalGroupValue = "Мужчина";
  final _items = ['Мужчина', 'Женщина'];

  void validateAndSave() {
    if (_formsKey.currentState!.validate()) {
      BlocProvider.of<SingupCubit>(context).singUpWithCredentials(email: _userNameController.text, password: _passwordController.text);
    } else {
      print('Form is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        centerTitle: true,
        leading: IconButton(
          color: Colors.black,
          icon: Icon(
            Icons.arrow_back_ios_new_outlined,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          'Создать аккаунт',
          style: TextStyle(
            color: black,
            fontSize: 18,
          ),
        ),
        elevation: 0.0,
      ),
      body: SafeArea(
        child: BlocConsumer<SingupCubit, SingupState>(
          listener: (context, state) {
            if (state is SingupFailure) {
              // ignore: deprecated_member_use
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.message),
                ),
              );
            }
            if (state is SingupSuccess) {
              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_) => MyHomePage()), (route) => false);
            }
          },
          builder: (context, state) {
            if (state is SingupLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }

            return Center(
              child: Padding(
                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                child: Form(
                  key: _formsKey,
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextFormField(
                          controller: _userNameController,
                          autofocus: true,
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            final regex =
                                RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

                            if (!regex.hasMatch(value!) || value.isEmpty) {
                              return 'Проверьте правильно ли введен email';
                            }
                          },
                          decoration: InputDecoration(
                            labelText: 'Электронная почта',
                            border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(10.0),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 24),
                        TextFormField(
                          controller: _passwordController,
                          autofocus: true,
                          textInputAction: TextInputAction.next,
                          obscureText: _isHide,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Проверьте правильно ли введены данные';
                            }
                            if (value.length < 6) {
                              return 'Пароль должен содержать больше шести знаков';
                            }
                          },
                          decoration: InputDecoration(
                            labelText: 'Пароль',
                            suffixIcon: InkWell(
                              onTap: () {
                                setState(() {
                                  if (_isHide) {
                                    _isHide = false;
                                  } else {
                                    _isHide = true;
                                  }
                                });
                              },
                              child: Icon(
                                _isHide == true ? Icons.visibility : Icons.visibility_off,
                              ),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(10.0),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 24),
                        TextFormField(
                          controller: _passwordController1,
                          autofocus: true,
                          textInputAction: TextInputAction.done,
                          obscureText: _isHide,
                          decoration: InputDecoration(
                            labelText: 'Введите пароль ещё раз',
                            suffixIcon: InkWell(
                              onTap: () {
                                setState(() {
                                  if (_isHide1) {
                                    _isHide1 = false;
                                  } else {
                                    _isHide1 = true;
                                  }
                                });
                              },
                              child: Icon(
                                _isHide == true ? Icons.visibility : Icons.visibility_off,
                              ),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(10.0),
                              ),
                            ),
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Проверьте правильно ли введены данные';
                            }
                            if (value.length < 6) {
                              return 'Пароль должен содержать больше шести знаков';
                            }

                            if (!_passwordController.text.contains(value)) {
                              return 'Пароли не совпадают!';
                            }
                          },
                        ),
                        SizedBox(height: 20),
                        RadioGroup<String>.builder(
                          horizontalAlignment: MainAxisAlignment.start,
                          direction: Axis.horizontal,
                          groupValue: _verticalGroupValue,
                          onChanged: (value) => setState(() {
                            _verticalGroupValue = value.toString();
                          }),
                          items: _items,
                          itemBuilder: (item) => RadioButtonBuilder(
                            item,
                          ),
                          activeColor: red,
                        ),
                        SizedBox(height: 50),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 50),
                          child: MaterialButton(
                            onPressed: () {
                              validateAndSave();
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(45),
                            ),
                            minWidth: MediaQuery.of(context).size.width,
                            height: 64,
                            color: mainColor,
                            child: Text(
                              'Создать аккаунт',
                              style: TextStyle(
                                color: white,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
