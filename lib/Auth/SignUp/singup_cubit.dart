import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_hackathon_application/Auth/Services/AuthService.dart';

import '/Models/User.dart';

part 'singup_state.dart';

class SingupCubit extends Cubit<SingupState> {
  final AuthService _authService;

  SingupCubit(this._authService) : super(SingupInitial());

  Future<void> singUpWithCredentials({required String email, required String password}) async {
    emit(SingupLoading());
    try {
      final user = await _authService.signUp(
        email: email,
        password: password,
      );
      emit(SingupSuccess(user));
    } catch (ex) {
      emit(SingupFailure('Похоже возникла какая-то ошибка, пожалуйста попробуйте позже'));
    }
  }
}
