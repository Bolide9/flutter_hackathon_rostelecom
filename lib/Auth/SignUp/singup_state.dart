part of 'singup_cubit.dart';

abstract class SingupState extends Equatable {
  const SingupState();

  @override
  List<Object> get props => [];
}

class SingupInitial extends SingupState {}

class SingupLoading extends SingupState {}

class SingupSuccess extends SingupState {
  final User? _user;

  const SingupSuccess(this._user);

  @override
  List<Object> get props => [_user != null] ;
}

class SingupFailure extends SingupState {
  final String message;

  const SingupFailure(this.message);

  @override
  List<Object> get props => [message];
}
