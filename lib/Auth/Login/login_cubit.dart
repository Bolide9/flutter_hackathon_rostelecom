import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_hackathon_application/Auth/Services/AuthService.dart';

import '/Models/User.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  final AuthService _authService;

  LoginCubit(this._authService) : super(LoginInitial());

  Future<void> logInWithCredentials({required String email, required String password}) async {
    emit(LoginLoading());
    try {
      final user = await _authService.logInWithEmailAndPassword(
        email: email,
        password: password,
      );
      emit(LoginSuccess(User(id: user.uid, email: user.email)));
    } catch (ex) {
      emit(LoginFailure('Похоже возникла какая-то ошибка, пожалуйста попробуйте позже'));
    }
  }

  Future logOut() async {
    try {
      await Future.wait([
        _authService.logOut(),
      ]);
    } on Exception {
      throw LogOutFailure();
    }
  }

  // Future<void> logInWithGoogle() async {
  //   emit(state.copyWith(status: FormzStatus.submissionInProgress));
  //   try {
  //     await _authenticationRepository.logInWithGoogle();
  //     emit(state.copyWith(status: FormzStatus.submissionSuccess));
  //   } on Exception {
  //     emit(state.copyWith(status: FormzStatus.submissionFailure));
  //   } on NoSuchMethodError {
  //     emit(state.copyWith(status: FormzStatus.pure));
  //   }
  // }
}
