part of 'auth_bloc.dart';

enum AppStatus {
  authenticated,
  unauthenticated,
}

class AppState extends Equatable {
  const AppState._({
    required this.status,
    this.user = model.User.empty,
  });

  const AppState.authenticated(model.User user) : this._(status: AppStatus.authenticated, user: user);

  const AppState.unauthenticated(model.User empty) : this._(status: AppStatus.unauthenticated);

  final AppStatus status;
  final model.User user;

  @override
  List<Object> get props => [status, user];
}
