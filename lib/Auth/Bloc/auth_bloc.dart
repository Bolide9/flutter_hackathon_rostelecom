import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import 'package:flutter_hackathon_application/Auth/Services/AuthService.dart' as firebaseUser;
import 'package:flutter_hackathon_application/Models/User.dart' as model;

part 'auth_event.dart';
part 'auth_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  AppBloc({required firebaseUser.AuthService authenticationRepository})
      : _authenticationRepository = authenticationRepository,
        super(authenticationRepository.currentUser == null
            ? AppState.authenticated(model.User(
                id: authenticationRepository.currentUser!.uid,
                name: authenticationRepository.currentUser!.displayName,
                email: authenticationRepository.currentUser!.email,
              ))
            : const AppState.unauthenticated(model.User.empty)) {
    _userSubscription = _authenticationRepository.user.listen(_onUserChanged);
  }

  final firebaseUser.AuthService _authenticationRepository;
  late final StreamSubscription _userSubscription;

  void _onUserChanged(dynamic user) => add(AppUserChanged(user));

  @override
  Stream<AppState> mapEventToState(AppEvent event) async* {
    if (event is AppUserChanged) {
      yield _mapUserChangedToState(event, state);
    } else if (event is AppLogoutRequested) {
      await _authenticationRepository.logOut();
    }
  }

  AppState _mapUserChangedToState(AppUserChanged event, AppState state) {
    return event.user.isNotEmpty ? AppState.authenticated(event.user) : const AppState.unauthenticated(model.User.empty);
  }

  @override
  Future<void> close() {
    _userSubscription.cancel();
    return super.close();
  }
}
