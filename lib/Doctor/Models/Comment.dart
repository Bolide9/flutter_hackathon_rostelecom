class Comment {
  String fullName;
  String comment;
  double rating;
  String date;

  Comment({
    required this.fullName,
    required this.comment,
    required this.rating,
    required this.date,
  });
}
