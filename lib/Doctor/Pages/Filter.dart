import 'package:flutter/material.dart';

import '../../Colors.dart';

class FilterPage extends StatefulWidget {
  FilterPage({Key? key}) : super(key: key);

  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  final TextEditingController _fromController = TextEditingController();

  bool _isOnline = true;
  bool _isOffline = true;

  bool _isM = true;
  bool _isF = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          'Фильтры',
          style: TextStyle(
            color: black,
          ),
        ),
        leading: IconButton(
          splashRadius: 25,
          icon: Icon(
            Icons.arrow_back_ios_new_outlined,
            color: black,
          ),
          onPressed: () {},
        ),
        actions: [
          TextButton(
            onPressed: () {},
            child: Text('Сбросить'),
          ),
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(left: 30, right: 30),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 24),
                TextField(
                  controller: _fromController,
                  autofocus: false,
                  decoration: InputDecoration(
                    labelText: 'Регион (город, страна)',
                    suffixIcon: IconButton(
                      color: black,
                      icon: Icon(
                        Icons.room_outlined,
                        color: black,
                      ),
                      onPressed: () {},
                      splashRadius: 25.0,
                    ),
                  ),
                ),
                SizedBox(height: 28),
                Text(
                  'Специальность',
                  style: TextStyle(
                    color: black,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 24),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.add_circle_outline_outlined,
                      color: lightBlue,
                    ),
                    SizedBox(width: 15),
                    Text(
                      'Добавить специальность',
                      style: TextStyle(
                        color: lightBlue,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 24),
                Divider(color: grey),
                SizedBox(height: 24),
                Text(
                  'Режим работы',
                  style: TextStyle(
                    color: black,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 24),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Онлайн консультация',
                      style: TextStyle(
                        color: black,
                        fontSize: 16,
                      ),
                    ),
                    Checkbox(
                      value: _isOnline,
                      onChanged: (value) {
                        setState(() {
                          _isOnline = value!;
                        });
                      },
                      activeColor: indigo,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Прием в больнице',
                      style: TextStyle(
                        color: black,
                        fontSize: 16,
                      ),
                    ),
                    Checkbox(
                      value: _isOffline,
                      onChanged: (value) {
                        setState(() {
                          _isOffline = value!;
                        });
                      },
                      activeColor: indigo,
                    ),
                  ],
                ),
                SizedBox(height: 18),
                Divider(color: grey),
                SizedBox(height: 18),
                Text(
                  'Пол  ',
                  style: TextStyle(
                    color: black,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 24),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Мужской',
                      style: TextStyle(
                        color: black,
                        fontSize: 16,
                      ),
                    ),
                    Checkbox(
                      value: _isM,
                      onChanged: (value) {
                        setState(() {
                          _isM = value!;
                        });
                      },
                      activeColor: indigo,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Женский',
                      style: TextStyle(
                        color: black,
                        fontSize: 16,
                      ),
                    ),
                    Checkbox(
                      value: _isF,
                      onChanged: (value) {
                        setState(() {
                          _isF = value!;
                        });
                      },
                      activeColor: indigo,
                    ),
                  ],
                ),
                SizedBox(height: 12),
                Divider(color: grey),
                SizedBox(height: 80),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                  child: MaterialButton(
                    onPressed: () {},
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(45),
                    ),
                    elevation: 0.0,
                    minWidth: MediaQuery.of(context).size.width,
                    height: 64,
                    color: mainColor,
                    child: Text(
                      'Показать результаты',
                      style: TextStyle(
                        color: white,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
