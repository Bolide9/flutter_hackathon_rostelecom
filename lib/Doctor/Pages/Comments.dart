import 'package:flutter/material.dart';
import 'package:flutter_hackathon_application/Doctor/Models/Comment.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../Colors.dart';

class Comments extends StatelessWidget {
  Comments({required this.doctorName});

  final String doctorName;

  final _list = [
    Comment(
      fullName: 'Медведев Д.',
      comment: 'Спасибо за консультацию! На все свои вопросы я получил исчерпывающие ответы и конкретный план действий.',
      rating: 5.0,
      date: '30.05.2021',
    ),
    Comment(
      fullName: 'Соловьев В.',
      comment: 'Все отлично, подробно, врач вежливый.',
      rating: 4.5,
      date: '12.05.2021',
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          doctorName,
          style: TextStyle(
            color: black,
            fontSize: 18,
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          splashRadius: 25,
          icon: Icon(
            Icons.arrow_back_ios_new_outlined,
            color: black,
          ),
          onPressed: () {},
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (context, int index) {
                final _item = _list[index];
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RatingBarIndicator(
                          rating: 2.75,
                          itemCount: 5,
                          itemBuilder: (context, index) => Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                          itemSize: 25.0,
                          direction: Axis.horizontal,
                        ),
                        Text(
                          _item.date.toString().trim(),
                          style: TextStyle(
                            color: grey,
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Text(
                      _item.fullName.toString(),
                      style: TextStyle(
                        color: black,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 24),
                    Text(
                      _item.comment.toString().trim(),
                      style: TextStyle(
                        color: black,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 24),
                    Divider(color: grey),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
