import 'package:flutter/material.dart';
import 'package:flutter_hackathon_application/Widgets/CustomGroupButtons.dart';

import '../../Colors.dart';

class MakeAppointment extends StatefulWidget {
  const MakeAppointment({Key? key}) : super(key: key);

  @override
  _MakeAppointmentState createState() => _MakeAppointmentState();
}

class _MakeAppointmentState extends State<MakeAppointment> {
  int _currentIndex = 0;
  int _selectedTime = 0;

  final day = DateTime.now().day;
  final year = DateTime.now().year;

  String nextDay = (DateTime.now().day + 1).toString();
  int month = DateTime.now().month.toInt();

  Iterable<TimeOfDay> getTimes(TimeOfDay startTime, TimeOfDay endTime, Duration step) sync* {
    var hour = startTime.hour;
    var minute = startTime.minute;

    do {
      yield TimeOfDay(hour: hour, minute: minute);
      minute += step.inMinutes;
      while (minute >= 60) {
        minute -= 60;
        hour++;
      }
    } while (hour < endTime.hour || (hour == endTime.hour && minute <= endTime.minute));
  }

  @override
  Widget build(BuildContext context) {
    Map<int, String> _monts = <int, String>{
      1: 'Январь',
      2: 'Февраль',
      3: 'Март',
      4: 'Апрель',
      5: 'Май',
      6: 'Июнь',
      7: 'Июль',
      8: 'Август',
      9: 'Сентябрь',
      10: 'Октябрь',
      11: 'Ноябрь',
      12: 'Декабрь',
    };

    String _getCurrentMonth = _monts[month].toString();

    final startTime = TimeOfDay(hour: 9, minute: 0);
    final endTime = TimeOfDay(hour: 22, minute: 0);
    final step = Duration(minutes: 30);

    final times = getTimes(startTime, endTime, step).map((tod) => tod.format(context)).toList();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text(
          'Записаться на прием',
          style: TextStyle(
            color: black,
            fontSize: 18,
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(
            Icons.arrow_back_ios_new_outlined,
            size: 24,
          ),
          color: grey,
        ),
      ),
      body: SafeArea(
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: CustomGroupButtons(
                      titles: [
                        'Сегодня',
                        'Завтра',
                        'Другая дата',
                      ],
                      currentIndex: _currentIndex,
                      onChanged: (int value) {
                        setState(
                          () {
                            _currentIndex = value;
                          },
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 24),
                  Padding(
                    padding: const EdgeInsets.only(left: 11.0),
                    child: Column(
                      children: [
                        _currentIndex == 0
                            ? Text(
                                '${day.toString()} ${_getCurrentMonth.toString()}',
                                style: TextStyle(
                                  color: black,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18,
                                ),
                              )
                            : Text(
                                '${nextDay.toString()} ${_getCurrentMonth.toString()}',
                                style: TextStyle(
                                  color: black,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18,
                                ),
                              ),
                      ],
                    ),
                  ),
                  SizedBox(height: 24),
                  GridView.builder(
                    padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    itemCount: 19,
                    itemBuilder: (context, int index) {
                      return Container(
                        margin: EdgeInsets.all(15),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              _selectedTime = index;
                            });
                          },
                          borderRadius: BorderRadius.circular(25.0),
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: grey,
                              ),
                              color: _selectedTime == index ? black : white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(25),
                              ),
                            ),
                            child: Text(
                              '${times[index].toString()}',
                              style: TextStyle(
                                color: _selectedTime == index ? white : black,
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
