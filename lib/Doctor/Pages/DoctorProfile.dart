import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hackathon_application/Call/Pages/CallPage.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../Colors.dart';
import 'MakeAppointment.dart';

// ignore: must_be_immutable
class DoctorProfile extends StatelessWidget {
  DoctorProfile({Key? key}) : super(key: key);

  FirebaseAuth _auth = FirebaseAuth.instance;

  // Widget _submitOnAppointment() {
  //   if (userId.toString() != null && userId.isNotEmpty) {
  //     return SfDateRangePicker();
  //   }

  //   return Container();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          'Психиатр',
          style: TextStyle(
            color: black,
            fontSize: 20,
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          splashRadius: 25,
          icon: Icon(
            Icons.arrow_back_ios_new_outlined,
            color: black,
          ),
          onPressed: () {},
        ),
        actions: [
          IconButton(
            splashRadius: 25,
            icon: Icon(
              Icons.share_outlined,
              color: black,
            ),
            onPressed: () {},
          ),
        ],
      ),
      body: SafeArea(
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // Container(
                //   child: Image.asset(name),
                // ),
                SizedBox(height: 16),
                Center(
                  child: Text(
                    'Бехтерев Владимир\nВладимирович',
                    textScaleFactor: 1.2,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(height: 32),
                Padding(
                  padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                  child: MaterialButton(
                    onPressed: () {
                      if (_auth.currentUser?.uid != null && !_auth.currentUser!.isAnonymous) {
                        print('valid');

                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => CallPage(),
                          ),
                        );
                      } else {
                        print('invalid');

                        Fluttertoast.showToast(
                          msg: 'Вы не авторизованы!',
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.SNACKBAR,
                          backgroundColor: Colors.grey.shade800,
                          textColor: Colors.white,
                          fontSize: 16.0,
                        );
                      }
                    },
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(45),
                    ),
                    minWidth: MediaQuery.of(context).size.width,
                    height: 55,
                    color: mainColor,
                    child: Text(
                      'Онлайн консультация',
                      style: TextStyle(
                        color: white,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 18),
                Padding(
                  padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MakeAppointment(),
                        ),
                      );
                    },
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(45),
                    ),
                    minWidth: MediaQuery.of(context).size.width,
                    height: 55,
                    color: greyLight,
                    child: Text(
                      'Запись на прием',
                      style: TextStyle(
                        color: black,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.only(left: 24.0, right: 24.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.star,
                            color: gold,
                          ),
                          SizedBox(width: 10),
                          Text(
                            4.9.toString(),
                            style: TextStyle(
                              color: grey,
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                      Text(
                        'Стаж  12 лет',
                      ),
                      TextButton(
                        onPressed: () {},
                        child: Text('Отзывы'),
                      ),
                    ],
                  ),
                ),
                Divider(color: grey),
                SizedBox(height: 24),
                Padding(
                  padding: const EdgeInsets.only(left: 24.0, right: 24.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Место приема',
                        style: TextStyle(
                          color: black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(height: 24),
                      Text(
                        '«Ренессанс клиника» ул. Казияв Али, 72, г. Санкт-Петербург',
                        style: TextStyle(
                          color: grey700,
                          fontWeight: FontWeight.normal,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(height: 24),
                      Text(
                        'Кабинет №34',
                        style: TextStyle(
                          color: grey700,
                          fontWeight: FontWeight.normal,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(height: 12),
                      Divider(color: grey),
                      SizedBox(height: 24),
                      Text(
                        'Образование',
                        style: TextStyle(
                          color: black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(height: 24),
                      Text(
                        'Высшая учебная категория. Окончил Военно-медицинскую академию имени С.М. Кирова в 2002г. специальности Терапия.',
                        style: TextStyle(
                          color: grey700,
                          fontWeight: FontWeight.normal,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(height: 12),
                      Divider(color: grey),
                    ],
                  ),
                ),
                SizedBox(height: 12),
                Padding(
                  padding: const EdgeInsets.only(left: 24.0, right: 24.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Образование',
                        style: TextStyle(
                          color: black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(height: 32),
                      ListView.builder(
                        itemCount: 3,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (context, int index) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                '2017 - 2018',
                                style: TextStyle(
                                  color: black,
                                  fontSize: 16,
                                ),
                              ),
                              SizedBox(height: 10),
                              Text(
                                'Врач-терапевт,',
                                style: TextStyle(
                                  color: grey700,
                                  fontSize: 16,
                                ),
                              ),
                              SizedBox(height: 5),
                              Text(
                                'АОА «Внуково»',
                                style: TextStyle(
                                  color: grey700,
                                  fontSize: 16,
                                ),
                              ),
                              SizedBox(height: 30),
                            ],
                          );
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 5),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
