import 'package:flutter/material.dart';

import '/Colors.dart';

class CustomGroupButtons extends StatelessWidget {
  CustomGroupButtons({
    Key? key,
    required this.currentIndex,
    required this.titles,
    required this.onChanged,
  }) : super(key: key);

  final int currentIndex;
  final List<String> titles;
  final ValueChanged<int> onChanged;

  // final _selectedColor = customIndigoColor;
  // final _unSelectedColor = Colors.white;

  // final _selectedTextColor = Colors.white;
  // final _unSelectedTextColor = Colors.black;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: _buttonList(),
    );
  }

  List<Widget> _buttonList() {
    var _buttons = <Widget>[];

    for (var i = 0; i < titles.length; i++) {
      _buttons.add(_button(titles[i], null, i));
    }

    return _buttons;
  }

  Widget _button(String title, IconData? icon, int index) {
    return Container(
      height: 55,
      margin: EdgeInsets.only(left: 3, right: 3),
      padding: EdgeInsets.zero,
      child: ElevatedButton(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all<Color>(currentIndex == index ? black : white),
          backgroundColor: MaterialStateProperty.all<Color>(currentIndex == index ? black : white),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0),
              side: BorderSide(color: currentIndex == index ? black : black),
            ),
          ),
        ),
        onPressed: () {
          onChanged(index);
        },
        child: Row(
          children: [
            icon != null
                ? Icon(
                    icon,
                    color: Colors.white,
                  )
                : Container(),
            icon != null
                ? SizedBox(
                    width: 7,
                  )
                : Container(),
            Text(
              title,
              style: TextStyle(
                color: currentIndex == index ? white : black,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
