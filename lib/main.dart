import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hackathon_application/Auth/Login/login_cubit.dart';
import 'package:flutter_hackathon_application/Onboarding/Pages/OnboardPage1.dart';

import '/Auth/Services/AuthService.dart';
import '/Auth/Bloc/auth_bloc.dart';
import '/Auth/SignUp/singup_cubit.dart';
import 'Doctor/Pages/MakeAppointment.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp],
  );

  final authenticationRepository = AuthService();

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => LoginCubit(authenticationRepository),
        ),
        BlocProvider(
          create: (_) => SingupCubit(authenticationRepository),
        ),
      ],
      child: MyApp(authenticationRepository: authenticationRepository),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
    required AuthService authenticationRepository,
  })  : _authenticationRepository = authenticationRepository,
        super(key: key);

  final AuthService _authenticationRepository;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Inter',
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (_) => AppBloc(
          authenticationRepository: _authenticationRepository,
        ),
        child: _authenticationRepository.currentUser?.uid == null ? OnboardPage1() : MakeAppointment(),
      ),
    );
  }
}
