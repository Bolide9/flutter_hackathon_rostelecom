import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hackathon_application/Auth/Login/LoginPage.dart';
import 'package:flutter_hackathon_application/Auth/SignUp/SignUpPage.dart';

import '../../Colors.dart';
import '../../Auth/Login/login_cubit.dart';
import '../../Onboarding/Pages/OnboardPage1.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  FirebaseAuth _auth = FirebaseAuth.instance;

  Widget _notAuth() {
    return Column(
      children: [
        SizedBox(height: 48),
        Image.asset(
          'assets/images/login_img.png',
          width: double.infinity,
        ),
        SizedBox(height: 48),
        Center(
          child: Text(
            'Похоже, вы не авторизованы',
            style: TextStyle(
              color: black,
              fontSize: 24,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 30, right: 30, top: 30, bottom: 10),
          child: MaterialButton(
            onPressed: () {
              Navigator.push(context, CupertinoPageRoute(builder: (context) => LoginPage()));
            },
            elevation: 0.0,
            color: mainColor,
            minWidth: MediaQuery.of(context).size.width,
            height: 60,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
                25,
              ),
            ),
            child: Text(
              'Вход',
              style: TextStyle(
                fontFamily: 'Inter',
                fontWeight: FontWeight.w600,
                fontSize: 16,
                color: white,
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 30, right: 30),
          child: TextButton(
            onPressed: () {
              Navigator.push(context, CupertinoPageRoute(builder: (context) => SingUpPage()));
            },
            child: Text(
              'Еще нет аккаунта',
              style: TextStyle(
                fontFamily: 'Inter',
                fontWeight: FontWeight.w600,
                fontSize: 16,
                color: black,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildBody() {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        title: Text(
          'Профиль',
          style: TextStyle(
            color: black,
            fontSize: 18,
          ),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {},
            splashRadius: 25.0,
            icon: Icon(
              Icons.settings_outlined,
              color: Colors.black,
              size: 28,
            ),
          ),
        ],
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.only(left: 24.0, right: 24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 48),
              // Container(
              //   child: Image,
              // ),
              Center(
                child: Text(
                  'Федоров Владимир\nДмитриевич',
                  textScaleFactor: 1.2,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
              ),
              SizedBox(height: 48),
              Text(
                'Основное',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),

              SizedBox(height: 28),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Возраст',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  Text(
                    '34',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: grey700,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),

              SizedBox(height: 28),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Пол',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  Text(
                    'Мужской',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: grey700,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),

              SizedBox(height: 28),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Рост',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  Text(
                    '182 см',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: grey700,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 28),
              Divider(
                color: grey,
              ),
              SizedBox(height: 24),
              TextButton(
                onPressed: () {
                  BlocProvider.of<LoginCubit>(context).logOut();
                  Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_) => OnboardPage1()), (route) => false);
                },
                child: Text('Выйти'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _auth.currentUser?.uid == null ? _notAuth() : _buildBody(),
    );
  }
}
