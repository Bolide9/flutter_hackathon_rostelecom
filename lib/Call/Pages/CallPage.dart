import 'package:flutter/material.dart';
import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';

class CallPage extends StatefulWidget {
  const CallPage({Key? key}) : super(key: key);

  @override
  _CallPageState createState() => _CallPageState();
}

class _CallPageState extends State<CallPage> {
  static const String app_id = 'b1fa737233bf4d1785ec7d8328c77cac';
  static const String token = '006b1fa737233bf4d1785ec7d8328c77cacIADMHdznTpopHl58phfKjHUJ/DorfSbm3PXhpoDDXni99epuE8wAAAAAEABFAsi6KVLPYAEAAQAnUs9g';

  int? _remoteUid;
  RtcEngine? _engine;

  @override
  void initState() {
    super.initState();
    initForAgora();
  }

  @override
  void dispose() {
    _engine!.leaveChannel();
    _engine!.destroy();
    super.dispose();
  }

  Future<void> _showToast({required String msg}) async {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Colors.grey.shade800,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  Future<void> initForAgora() async {
    await [Permission.microphone, Permission.camera].request();

    // _engine = await RtcEngine.createWithConfig(RtcEngineConfig(app_id));
    _engine = await RtcEngine.create(app_id);

    await _engine?.enableVideo();

    _engine!.setEventHandler(
      RtcEngineEventHandler(
        networkQuality: (uid, networkQuality, networkQualit) {},
        userInfoUpdated: (uid, userEmail) {
          print(userEmail.userAccount);
        },
        joinChannelSuccess: (String channel, int uid, int elapsed) {
          _showToast(
            msg: "Вы присоединились к звонку",
          );
        },
        userJoined: (int uid, int elapsed) {
          setState(() {
            _remoteUid = uid;
          });
        },
        userOffline: (int uid, UserOfflineReason reason) {
          _showToast(
            msg: "Пользователь отключился",
          );
          setState(() {
            _remoteUid = null;
          });
        },
      ),
    );

    await _engine!.joinChannel(token, "testchannel", null, 0);
  }

  Widget _renderLocalVideo() {
    return RtcLocalView.SurfaceView();
  }

  Widget _renderRemoteVideo() {
    if (_remoteUid != null) {
      return RtcRemoteView.SurfaceView(uid: _remoteUid!);
    } else {
      return Text(
        'Пожалуйста подождите пока кто-то подключиться',
        textAlign: TextAlign.center,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Center(
              child: _renderRemoteVideo(),
            ),
            Container(
              padding: EdgeInsets.only(left: 10, top: 20),
              child: Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 100,
                  height: 100,
                  child: Center(
                    child: _renderLocalVideo(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
