import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hackathon_application/Home/HomePage.dart';

import '../Models/View.dart';

import '../../Colors.dart';
import '../../Auth/Login/LoginPage.dart';
import '../../Auth/SignUp/SignUpPage.dart';

class OnboardPage1 extends StatefulWidget {
  OnboardPage1({Key? key}) : super(key: key);

  @override
  _OnboardPage1State createState() => _OnboardPage1State();
}

class _OnboardPage1State extends State<OnboardPage1> {
  late int _current;
  late List<View> _views;
  late List<Widget> _list;

  @override
  void initState() {
    super.initState();

    _current = 0;

    _views = [
      View(
        imgUrl: 'assets/images/onboard1.png',
        title: 'Добро пожаловать',
        subTitle: 'У нас вы можете записаться на прием к врачу онлайн!',
      ),
      View(
        imgUrl: 'assets/images/onboard2.png',
        title: 'Онлайн консультация с врачом',
        subTitle: 'Получайте онлайн-консультацию не выходя из дома!',
      ),
      View(
        imgUrl: 'assets/images/onboard4.png',
        title: 'Запишитесь на онлайн прием',
        subTitle: 'Вы можете записать на онлайн прием к врачу в удобное для вас время!',
      ),
    ];

    _list = _views
        .map((item) => Column(
              children: [
                Image.asset(
                  item.imgUrl,
                  fit: BoxFit.contain,
                ),
                SizedBox(height: 24),
                Padding(
                  padding: const EdgeInsets.only(left: 5.0, right: 5.0),
                  child: Text(
                    item.title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 24,
                      color: black,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                SizedBox(height: 12),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Text(
                    item.subTitle,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: grey700,
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ],
            ))
        .toList();
  }

  Widget _buildBody({required List<View> views, required String btnText, required String btnText2, required BuildContext context}) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              CarouselSlider(
                items: _list,
                options: CarouselOptions(
                  height: 600,
                  disableCenter: false,
                  viewportFraction: 1,
                  initialPage: _current,
                  enableInfiniteScroll: true,
                  reverse: false,
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 5),
                  autoPlayAnimationDuration: Duration(milliseconds: 700),
                  autoPlayCurve: Curves.fastOutSlowIn,
                  enlargeCenterPage: true,
                  scrollDirection: Axis.horizontal,
                  onPageChanged: (index, reason) {
                    setState(
                      () {
                        _current = index;
                      },
                    );
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: _views.map((url) {
                  int index = _views.indexOf(url);
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin: EdgeInsets.only(left: 2, right: 2),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == index ? mainColor : Colors.grey.shade300,
                    ),
                  );
                }).toList(),
              ),
            ],
          ),
          SizedBox(height: 15),
          Container(
            padding: EdgeInsets.only(left: 30, right: 30),
            child: MaterialButton(
              onPressed: () {
                Navigator.push(context, CupertinoPageRoute(builder: (context) => SingUpPage()));
              },
              elevation: 0.0,
              color: mainColor,
              minWidth: MediaQuery.of(context).size.width,
              height: 60,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  25,
                ),
              ),
              child: Text(
                'Создать аккаунт',
                style: TextStyle(
                  fontFamily: 'Inter',
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                  color: white,
                ),
              ),
            ),
          ),
          SizedBox(height: 5),
          TextButton(
            onPressed: () {
              Navigator.push(context, CupertinoPageRoute(builder: (context) => LoginPage()));
            },
            child: Text(
              'Войти',
              style: TextStyle(
                fontFamily: 'Inter',
                fontWeight: FontWeight.w600,
                fontSize: 16,
                color: mainColor,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: (context) => MyHomePage(),
                  ),
                );
              },
              child: Text(
                'Мне просто спросить...',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                  color: black,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 5),
              _buildBody(
                views: _views,
                btnText: 'Войти',
                btnText2: 'Создать аккаунт',
                context: context,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
