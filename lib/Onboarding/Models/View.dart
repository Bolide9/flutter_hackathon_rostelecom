class View {
  String imgUrl;
  String title;
  String subTitle;

  View({
    required this.imgUrl,
    required this.title,
    required this.subTitle,
  });
}
