import 'package:firebase_auth/firebase_auth.dart';

FirebaseAuth auth = FirebaseAuth.instance;

String get userEmail => auth.currentUser!.email.toString();

String get userId => auth.currentUser!.uid.toString();
